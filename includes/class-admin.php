<?php
namespace Calculator;

/**
 * Admin Pages Handler
 */
class Admin {

	public function __construct() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_filter( 'manage_calculator_posts_columns', [ $this, 'set_post_columns' ] );
		add_action( 'manage_calculator_posts_custom_column', [ $this, 'set_columns_content' ], 10, 2 );
	}

	/**
	 * Inject our metabox into the clickbot post type
	 *
	 * @return void
	 */
	public function admin_menu() {


	}

	/**
	 * Add custom columns
	 *
	 * @return Array
	 */
	public function set_post_columns( $columns ) {
		$columns = [
			'cb'        => $columns['cb'],
			'title'     => 'Title',
			'type'      => 'Type',
			'shortcode' => 'Shortcode',
			'date'      => $columns['date'],
		];

		return $columns;
	}

	/**
	 * Set columns content
	 */
	public function set_columns_content( $column, $id ) {

		$selected_type = get_post_meta( $id, 'calculator_type', true );

		switch ( $column ) {

			case 'type':
				echo esc_html( $selected_type );
				break;

			case 'shortcode':
				echo "[calculator id=\"$id\"]";
				break;
		}
	}

}
