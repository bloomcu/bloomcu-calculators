<?php
namespace Calculator;

/**
 * REST_API Handler
 */
class Ajax {

	public function __construct() {
		add_action( 'wp_ajax_calculator_save_calculator', [ $this, 'save_calculator' ] );
	}

	public function save_calculator() {

		$post_id   = sanitize_text_field( $_POST['id'] );
		$calculator_type = sanitize_text_field( $_POST['type'] );
		$calculator_json = $_POST['schema'];

		if ( empty( $post_id ) || empty( $calculator_json ) ) {
			return;
		}

		$this->save_postdata( $post_id, $calculator_json, $calculator_type );

		wp_die();

	}

	/**
	 * Save Quiz Data
	 */
	public function save_postdata( $post_id, $schema, $type ) {

		update_post_meta(
			$post_id,
			'_bloom_calculator_schema',
			wp_slash( $schema )
		);

		update_post_meta(
			$post_id,
			'_bloom_calculator_type',
			$type
		);

	}

}
