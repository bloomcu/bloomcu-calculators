<?php
/**
 * Index
 * Entry point for all utilities.
 *
 */

$files = [
    'bcu_calc_get_options.php',
];

foreach ( $files as $file ) {
	require_once $file;
}
