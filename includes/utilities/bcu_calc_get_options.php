<?php
/**
 * Get options
 * Fetches data needed to setup dropdowns and button groups
 *
 * @param  boolean $id - Calculator ID
 * @param  string $type - Type of field
 * @param  string $fieldname - ACF field with options
 *
 * @return array
 */

function bcu_calc_get_options( $id, $part, $type) {

    /**
	* Dropdown or Buttons
	*/
    if ( $type == 'Dropdown' || $type == 'Buttons' ) {

        // Setup ACF fieldname
        $fieldname = $part . '_options';

        // Expecting ACF group
        // Check if the group field has rows of data
        if( have_rows( $fieldname, $id ) ) {

            $array = array(); // Empty array to hold our data

            $i = 0; // Incrementer

            // Loop through the rows of data and add to our array
            while ( have_rows( $fieldname, $id ) ) {
                the_row();
                $array[$i]['value'] = get_sub_field('value');
                $array[$i]['label'] = get_sub_field('label');
                $i++;
            }

            return $array;

        }

    }

    /**
	* Slider
	*/
    elseif ( $type == 'Slider' ) {

        // Setup ACF fieldname
        $fieldname = $part . '_slider-options';

        // Expecting ACF repeater
        // Check if the repeater field has rows of data
        if( have_rows( $fieldname, $id ) ) {

            $array = array(); // Empty array to hold our data

            // Loop through the rows of data and add to our array
            while ( have_rows( $fieldname, $id ) ) {
                the_row();
                $array['min'] = get_sub_field('min');
        		$array['max'] = get_sub_field('max');
        		$array['step'] = get_sub_field('step');
        		$array['val'] = get_sub_field('val');
            }

            return $array;

        }

    }

} ?>
