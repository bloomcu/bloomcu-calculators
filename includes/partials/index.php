<?php
/**
 * Index
 * Entry point for all partials.
 *
 */

$files = [
    'layout-builder.php',
    'layouts/header/index.php',
    'layouts/body/index.php',
    'layouts/footer/index.php',
    'footer.php',
    'header.php',
    'inputs.php',
    'modal.php',
    'slider-scripts.php'
];

foreach ( $files as $file ) {
	require_once $file;
}
