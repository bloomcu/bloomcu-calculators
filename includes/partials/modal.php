<?php
/**
 * Modal
 *
 */

function render_modal( $modal_header, $modal_content, $modal_location ) { 
    $idRand__modal = mt_rand(10000,99999);
    ?>

    <a class="calculator-modal-trigger" href="#calculator-<?php echo $modal_location; ?>-modal-<?php echo $idRand__modal; ?>" 
    aria-labelledby="calculator-<?php echo $modal_location; ?>-modal-header-<?php echo $idRand__modal; ?>">
        <svg width="18px" height="18px" viewBox="0 0 90 90">
            <g id="icon-question__<?php echo $modal_location . '-'.$idRand__modal;?>" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <path d="M45,0 C20.182716,0 0,20.18272 0,45 C0,69.8173 20.182716,90 45,90 C69.817284,90 90,69.8173 90,45 C90,20.18272 69.817284,0 45,0 Z M45,6 C66.574644,6 84,23.42536 84,45 C84,66.5746 66.574644,84 45,84 C23.425356,84 6,66.5746 6,45 C6,23.42536 23.425356,6 45,6 Z M45,20 C36.751258,20 30,26.75127 30,35 C29.9846991,36.0819077 30.5531259,37.0882441 31.4876185,37.6336632 C32.4221112,38.1790822 33.5778888,38.1790822 34.5123815,37.6336632 C35.4468741,37.0882441 36.0153009,36.0819077 36,35 C36,29.99391 39.993902,26 45,26 C50.006098,26 54,29.99391 54,35 C54,38.11324 52.744892,40.23848 50.6875,42.40625 C48.630108,44.574 45.775527,46.53585 43.125,48.65625 C42.413419,49.2260266 41.999474,50.088412 42,51 L42,55 C41.9846991,56.0819077 42.5531259,57.0882441 43.4876185,57.6336632 C44.4221112,58.1790822 45.5778888,58.1790822 46.5123815,57.6336632 C47.4468741,57.0882441 48.0153009,56.0819077 48,55 L48,52.46875 C50.185751,50.80025 52.752652,48.96505 55.0625,46.53125 C57.755108,43.69425 60,39.86747 60,35 C60,26.75127 53.248742,20 45,20 Z M45,62 C42.79086,62 41,63.7908 41,66 C41,68.2091 42.79086,70 45,70 C47.20914,70 49,68.2091 49,66 C49,63.7908 47.20914,62 45,62 Z" id="Shape__<?php echo $modal_location . '-'.$idRand__modal;?>" fill="#000000" fill-rule="nonzero"></path>
            </g>
        </svg>
    </a>
    <?php add_action('wp_footer', function() use ( $modal_header, $modal_content, $modal_location, $idRand__modal ) {
			ob_start();
            include( CALCULATORPLUGIN_INCLUDES . '/partials/layouts/modals/loan-rate.php' );
			echo ob_get_clean();
		});
} ?>
