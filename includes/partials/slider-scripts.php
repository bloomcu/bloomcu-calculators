<?php 
function render_slider_scripts() { ?>
    <script type="text/javascript">
		var sections = document.getElementsByClassName("calculator-section");
		for ( var i = 0; i<sections.length; i++ ) {
			if( sections[i].getElementsByClassName("slider_input").length ){
				var slider = sections[i].getElementsByClassName("slider_input")[0];
				var output = sections[i].getElementsByClassName("slider-output")[0];
				output.innerHTML = slider.value;
				bindSlider(slider, output);
				
			}
			
		}
		function bindSlider(slider, output){
			slider.oninput = function() {
				output.innerHTML = this.value;
			}
			slider.onchange = function() {
				output.innerHTML = this.value;
			}
		}
		// var slider = document.getElementById("slider_input");
		// var output = document.getElementById("slider_output");
		
		// output.innerHTML = slider.value; // Display the default slider value


		// Update the current slider value (each time you drag the slider handle)
		// slider.oninput = function() {

		// 	output.innerHTML = this.value;
		// }
		// slider.onchange = function() {
		// 	output.innerHTML = this.value;
		// }
	</script>
<?php } 
