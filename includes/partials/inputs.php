<?php
/**
 * Inputs
 *
 */

function render_input( $id, $part, $mainLabel, $type, $value, $options ) { 

	$isRate = $part == 'rate';
	
	//randomID
	$idRand__main = mt_rand(10000,99999);
	$input_rand_ID = mt_rand(10000,99999);
	
	$mainLabel_ID = 'calc-'.$id.'-'.$part.'-'.$idRand__main;
 ?>

	<div class="calculator-section <?php echo  $type == 'Slider' ? 'calculator-section--has-slider' : '';?>">

			<div class="calc_main_input_desc">
				<span id="<?php echo $mainLabel_ID; ?>" >
					<?php echo $mainLabel; ?>
				</span>

				<?php if ( $isRate && get_post_meta( $id, 'rate_modal_content', true ) ) {

                    /**
            		* Rate Modal
            		* ----------
            		*/

            		render_modal(
                        $modal_header = get_post_meta( $id, 'rate_modal_header', true ),
                        $modal_content = get_post_meta( $id, 'rate_modal_content', true ),
                        $modal_location = 'rate'
                    );

        		} ?>
			</div>

			<?php
			/**
			* Input: Text
			*/
			if ( $type == 'Text'): 

			?>

			<div class="input-group">
				<?php if ( $part == 'amount') { ?>
					<div class="pre">$</div>
				<?php } elseif ( $part == 'rate') { ?>
					<div class="pre">%</div>
				<?php } ?>
				<input type="text" class="calc-input input-<?php echo $part; ?>" value="<?php echo $value; ?>" 
				id="calc-<?php echo $id;?>-<?php echo $part;?>-<?php echo $input_rand_ID;?>" 
				<?php echo $isRate ? "maxlength='5'" :"";?>
				aria-labelledby="<?php echo $mainLabel_ID; ?>">
			</div>

			<?php
			/**
			* Select: Dropdown
			*/
			elseif ( $type == 'Dropdown'): 
			?>

				<div class="select-group">
					<select class="calc-input input-<?php echo $part; ?>" 
					id="calc-<?php echo $id;?>-<?php echo $part;?>-<?php echo $input_rand_ID;?>" 
					aria-labelledby="<?php echo $mainLabel_ID; ?>">

					<?php foreach ( $options as $option ) : ?>
						<option value="<?php echo $option['value']; ?>"><?php echo $option['label']; ?></option>
					<?php endforeach; ?>

					</select>
				</div>

			<?php
			/**
			* Select: Buttons
			*/
			elseif ( $type == 'Buttons'): 
			
			?>

				<div class="button-group" 
				id="calc-<?php echo $id;?>-<?php echo $part;?>-<?php echo $input_rand_ID;?>">
					<fieldset>
					<legend class="hide h-visual-hide"><?php echo $mainLabel; ?></legend>

						<?php
						$i = 1;
						foreach ( $options as $option ) : 
						
							$input_button_ID = 'calc-'.$id.'__input-'.$part.'-'.$i.'-'.$input_rand_ID;
							$input_button_label_ID = 'calc-'.$id.'-'.$part.'-'.$i.'-'.$input_rand_ID;
							$input_button_classes = 'calc-input input-'.$part;

						?>
							<span class="button-group__button">
							    <input 
									value="<?php echo $option['value']; ?>"
									type="radio"
									name="<?php echo $part; ?>-options-<?php echo $id;?>"
									class="<?php echo $input_button_classes; ?>"
									aria-labelledby="<?php echo $input_button_ID; ?>_label"
									id="<?php echo $input_button_ID; ?>"
									<?php if ( $i == 1 ) { echo 'checked'; }  ?>
								>
							    <label 
								id="<?php echo $input_button_ID; ?>_label" 
								for="<?php echo $input_button_ID; ?>"
								>
								<?php echo $option['label']; ?>
								</label>
							</span>
						<?php $i++; endforeach; ?>

					</fieldset>
				</div>

			<?php
			/**
			* Input: Slider
			*/
			// $part, $mainLabel, $type, $value, $options
			elseif ( $type == 'Slider'): ?>

				<span id="slider_output_<?php echo $part.'-'.$input_rand_ID;?>" class="slider-output slider-output-<?php echo $part; ?>"></span>

				<div class="slider-group">
					<input type="range"
						id="slider_input_<?php echo $input_rand_ID;?>"
						class="calc-input slider_input input-<?php echo $part; ?>"
						name="slider"
						min="<?php echo $options['min']; ?>"
						max="<?php echo $options['max']; ?>"
						step="<?php echo $options['step']; ?>"
						value="<?php echo $options['val']; ?>"
						aria-labelledby="<?php echo $mainLabel_ID;?>">
						<div class="slider-group-min slider-group-min-<?php echo $part; ?>">
							<?php echo $options['min']; ?>
						</div>
						<div class="slider-group-max slider-group-min-<?php echo $part; ?>">
							<?php echo $options['max']; ?>
						</div>
				</div>



			<?php endif; ?>

	</div>

	<?php
} ?>
