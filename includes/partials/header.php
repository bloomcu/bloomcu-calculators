<?php
/**
 * Header
 *
 */

function render_header( $title, $subtitle = false ) { ?>

    <div class="calculator-header">
        <p class="calculator-header__title"><?php echo $title; ?></p>
        <div class="output-result"></div>

        <?php if( $subtitle ):?>
          <span class="calculator-header__subtitle"><?php echo $subtitle;?></span>
        <?php endif;?>
    </div>

	<?php
} ?>
