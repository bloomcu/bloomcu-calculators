<?php
/**
 * Footer
 *
 */

function render_footer( $id, $title, $btn_label, $btn_url, $disclosure ) { ?>

    <?php if ( $title || $btn_label || $disclosure ) { ?>

        <div class="calculator-footer">

    		<?php if ( $title ) { ?>
            	<p><?php echo $title; ?></p>
    		<?php } ?>

            <?php if ( $btn_url ) { ?>
                <a class="button" href="<?php echo $btn_url; ?>"><?php echo $btn_label; ?></a>
            <?php } ?>

            <p class="calculator-disclosure">

                <?php echo $disclosure; ?>

                <?php if ( get_post_meta( $id, 'footer_modal_content', true ) ) {

                    /**
            		* Modal
            		* ----------
            		*/

            		render_modal(
                        $modal_header = get_post_meta( $id, 'footer_modal_header', true ),
                        $modal_content = get_post_meta( $id, 'footer_modal_content', true ),
                        $modal_location = 'footer'
                    );

        		} ?>

            </p>

        </div>

    <?php } ?>

	<?php
} ?>
