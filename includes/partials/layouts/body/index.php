<?php $includes = [
    'loan-payment.php',
    'loan-affordability.php',
    'mortgage-payment.php',
    'savings-compounding.php'
];

foreach( $includes as $include ) {
    require_once( $include );
}