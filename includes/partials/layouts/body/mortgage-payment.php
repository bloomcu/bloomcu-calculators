<?php function get_mortgage_payment_body_layout($id, $idRand = NULL) { ?>
  <div class="calculator-body">

  <?php
  /**
  * Amount
  * ----------
  */

  if ( get_post_meta( $id, 'amount_customize', true ) ) {

      render_input(
          $id,
          $part    = 'amount',
          $label   = get_post_meta( $id, 'amount_label', true ),
          $type    = get_post_meta( $id, 'amount_type', true ),
          $value   = get_post_meta( $id, 'amount_placeholder', true ),
          $options = bcu_calc_get_options( $id, $part, $type, true )
      );

  } else {

      render_input(
          $id,
          $part    = 'amount',
          $label   = 'Mortgage Amount',
          $type    = 'Text',
          $value   = '200000',
          $options = ''
      );

  }

  ?>

  <div class="calculator-section">
  
  <?php $input_down_payment = 'input-down-payment-'.$idRand;?>

      <label for="<?php echo $input_down_payment;?>"
      class="calc_main_input_desc"
      ><?php echo __('Down Payment', 'bloomcu-calculators');?></label>
      <div class="input-group">
          <div class="pre">$</div>
          <input type="text" class="calc-input input-down-payment" value="40000"
          id="<?php echo $input_down_payment;?>">
      </div>
  </div>

  <?php

  /**
  * Rate
  * ----------
  */

  if ( get_post_meta( $id, 'rate_customize', true ) ) {

      render_input(
          $id,
          $part    = 'rate',
          $label   = get_post_meta( $id, 'rate_label', true ),
          $type    = get_post_meta( $id, 'rate_type', true ),
          $value   = get_post_meta( $id, 'rate_placeholder', true ),
          $options = bcu_calc_get_options( $id, $part, $type, true )
      );

  } else {

      render_input(
          $id,
          $part    = 'rate',
          $label   = 'Fixed Rate',
          $type    = 'Text',
          $value   = '4.00',
          $options = ''
      );

  }

  /**
  * Term
  * ----------
  */

  if ( get_post_meta( $id, 'term_customize', true ) ) {

      render_input(
          $id,
          $part    = 'term',
          $label   = get_post_meta( $id, 'term_label', true ),
          $type    = get_post_meta( $id, 'term_type', true ),
          $value   = get_post_meta( $id, 'term_placeholder', true ),
          $options = bcu_calc_get_options( $id, $part, $type, true )
      );

  } else {

      render_input(
          $id,
          $part    = 'term',
          $label   = 'Term Years',
          $type    = 'Dropdown',
          $value   = '',
          $options = array(
              array(
                  "value"=>"30",
                  "label"=>"30 Years"
              ),
              array(
                  "value"=>"25",
                  "label"=>"25 Years"
              ),
              array(
                  "value"=>"20",
                  "label"=>"20 Years"
              ),
              array(
                  "value"=>"15",
                  "label"=>"15 Years"
              ),
              array(
                  "value"=>"10",
                  "label"=>"10 Years"
              )
          )
      );

  }

  ?>

  <a class="calculator-modal-trigger calculator-disclosure" href="#calculator-mortgage-advanced-<?php echo $idRand; ?>">
    <?php echo __('Advanced Options', 'bloomcu-calculators');?>
      
  </a>
</div>

<?php }