<?php function get_loan_payment_body_layout($id) { ?>
	<div class="calculator-body">

	 <?php

	 /**
	 * Amount
	 * ----------
	 */

	 if ( get_post_meta( $id, 'amount_customize', true ) ) {
		 render_input(
			 $id,
			 $part    = 'amount',
			 $label   = get_post_meta( $id, 'amount_label', true ),
			 $type    = get_post_meta( $id, 'amount_type', true ),
			 $value   = get_post_meta( $id, 'amount_placeholder', true ),
			 $options = bcu_calc_get_options( $id, $part, $type, true )
		 );

	 } else {

		 render_input(
			 $id,
			 $part    = 'amount',
			 $label   = 'Loan Amount',
			 $type    = 'Text',
			 $value   = '10000',
			 $options = ''
		 );

	 }

	 /**
	 * Rate
	 * ----------
	 */

	 if ( get_post_meta( $id, 'rate_customize', true ) ) {

		 render_input(
			 $id,
			 $part    = 'rate',
			 $label   = get_post_meta( $id, 'rate_label', true ),
			 $type    = get_post_meta( $id, 'rate_type', true ),
			 $value   = get_post_meta( $id, 'rate_placeholder', true ),
			 $options = bcu_calc_get_options( $id, $part, $type, true )
		 );

	 } else {

		 render_input(
			 $id,
			 $part    = 'rate',
			 $label   = 'Rate',
			 $type    = 'Text',
			 $value   = '3.50',
			 $options = ''
		 );

	 }

	 /**
	 * Term
	 * ----------
	 */

	 if ( get_post_meta( $id, 'term_customize', true ) ) {

		 render_input(
			 $id,
			 $part    = 'term',
			 $label   = get_post_meta( $id, 'term_label', true ),
			 $type    = get_post_meta( $id, 'term_type', true ),
			 $value   = get_post_meta( $id, 'term_placeholder', true ),
			 $options = bcu_calc_get_options( $id, $part, $type, true )
		 );

	 } else {

		 render_input(
			 $id,
			 $part    = 'term',
			 $label   = 'Term Months',
			 $type    = 'Dropdown',
			 $value   = '',
			 $options = array(
				 array(
					 "value"=>"72",
					 "label"=>"72 Months"
				 ),
				 array(
					 "value"=>"60",
					 "label"=>"60 Months"
				 ),
				 array(
					 "value"=>"48",
					 "label"=>"48 Months"
				 ),
				 array(
					 "value"=>"36",
					 "label"=>"36 Months"
				 )
			 )
		 );

	 }

	 ?>
	 </div>
 <?php }