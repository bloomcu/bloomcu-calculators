<?php function get_savings_compounding_body_layout($id, $idRand = NULL) { ?>
    <div class="calculator-body" data-term-as="<?php echo get_post_meta( $id, 'calculate_term_as', true );?>">

        <?php

        /**
        * Amount
        * ----------
        */

        if ( get_post_meta( $id, 'amount_customize', true ) ) {

            render_input(
                $id,
                $part    = 'amount',
                $label   = get_post_meta( $id, 'amount_label', true ),
                $type    = get_post_meta( $id, 'amount_type', true ),
                $value   = get_post_meta( $id, 'amount_placeholder', true ),
                $options = bcu_calc_get_options( $id, $part, $type, true )
            );

        } else {

            render_input(
                $id,
                $part    = 'amount',
                $label   = 'Initial Deposit',
                $type    = 'Text',
                $value   = '1000',
                $options = ''
            );

        }

        /**
        * Rate
        * ----------
        */

        if ( get_post_meta( $id, 'rate_customize', true ) ) {

            render_input(
                $id,
                $part    = 'rate',
                $label   = get_post_meta( $id, 'rate_label', true ),
                $type    = get_post_meta( $id, 'rate_type', true ),
                $value   = get_post_meta( $id, 'rate_placeholder', true ),
                $options = bcu_calc_get_options( $id, $part, $type, true )
            );

        } else {

            render_input(
                $id,
                $part    = 'rate',
                $label   = 'Interest Rate',
                $type    = 'Text',
                $value   = '3.00',
                $options = ''
            );

        }

        ?>

        <div class="calculator-section">
            <label for="calc-<?php echo $id;?>-monthly-contribution-<?php echo $idRand; ?>" 
            class="calc_main_input_desc"
            ><?php echo __('Monthly Contribution', 'bloomcu-calculators');?>
            </label>
            <div class="input-group" >
                <div class="pre">$</div>
                <input type="text" class="calc-input input-monthly-contribution" value="0" id="calc-<?php echo $id;?>-monthly-contribution-<?php echo $idRand; ?>" name="calc-<?php echo $id;?>-monthly-contribution">
            </div>
        </div>

        <?php

        /**
        * Term
        * ----------
        */

        if ( get_post_meta( $id, 'term_customize', true ) ) {

            render_input(
                $id,
                $part    = 'term',
                $label   = get_post_meta( $id, 'term_label', true ),
                $type    = get_post_meta( $id, 'term_type', true ),
                $value   = get_post_meta( $id, 'term_placeholder', true ),
                $options = bcu_calc_get_options( $id, $part, $type, true )
            );

        } else {

            render_input(
                $id,
                $part    = 'term',
                $label   = 'Months',
                $type    = 'Slider',
                $value   = '',
                $options = array(
                    "min"=>"3",
                    "max"=>"60",
                    "step"=>"3",
                    "val"=>"48",
                )
            );

        }

        ?>

    </div>
<?php }