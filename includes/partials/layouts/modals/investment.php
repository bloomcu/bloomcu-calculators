<div
    id="calculator-retirement-modal-<?php echo $idRand; ?>"
    class="c-off-canvas c-off-canvas--small c-off-canvas--calculator"
    data-modal="calculator-retirement-modal-<?php echo $idRand; ?>"
    aria-hidden="true"
>
    <div class="c-off-canvas__inner">
        <button class="c-off-canvas__close" data-close-modal aria-label="Close this information">
            <span class="c-off-canvas__close-icon">
                <i class="fal fa-times"></i>
            </span>
        </button>

        <div class="c-off-canvas__content">
            <h3 class="c-off-canvas__title">
                Info & assumptions
            </h3>

            <div class="c-off-canvas__content-inner wysiwyg-content">
                <h4>Inflation</h4>
                <p>Calculation does not account for inflation. This is a common measure of inflation in the U.S. is the Consumer Price Index (CPI). From 1925 through 2018 the CPI has a long-term average of 2.9% annually.</p>

                <h4>Tax rate</h4>
                <p>Calculation does not account for annual taxes.</p>

                <h4>Rate of return</h4>
                <p>Interest compounds yearly.</p>

                <h4>Disclosure</h4>
                <p>Information and interactive calculators are made available to you as self-help tools for your independent use and are not intended to provide investment advice. We cannot and do not guarantee their applicability or accuracy in regards to your individual circumstances. All examples are hypothetical and are for illustrative purposes. We encourage you to seek personalized advice from qualified professionals regarding all personal finance issues.</p>
            </div>
        </div>
    </div>
</div>