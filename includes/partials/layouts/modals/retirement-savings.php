<div
        id="calculator-retirement-modal-<?php echo $idRand; ?>"
        class="c-off-canvas c-off-canvas--small c-off-canvas--calculator"
        data-modal="calculator-retirement-modal-<?php echo $idRand; ?>"
        aria-hidden="true"
    >
        <div class="c-off-canvas__inner">
            <button class="c-off-canvas__close" data-close-modal aria-label="Close this information">
                <span class="c-off-canvas__close-icon">
                    <i class="fal fa-times"></i>
                </span>
            </button>

            <div class="c-off-canvas__content">
                <h3 class="c-off-canvas__title">
                    Assumptions
                </h3>

                <div class="c-off-canvas__content-inner wysiwyg-content">
                    <h4>Retirement age</h4>
                    <p>Calculation assumes that the year you retire, you don't make contributions. If you retire at 67, the calculation assumes your last contribution is at 66.</p>

                    <h4>Inflation</h4>
                    <p>Calculation assumes a 2.9% rate of inflation. This is a common measure of inflation in the U.S. is the Consumer Price Index (CPI). From 1925 through 2018 the CPI has a long-term average of 2.9% annually.</p>

                    <h4>Social Security</h4>
                    <p>Social Security benefits are not considered for this calculation. Social Security is based on a sliding scale depending on your income, how long you work and at what age you retire.</p>

                    <h4>Pensions</h4>
                    <p>Pensions are not considered for this calculation.</p>

                    <h4>Rate of return</h4>
                    <p>Interest compounds monthly. The interest rate specified in calculator is used to calculate your return on savings and investments during and after retirement.</p>

                    <h4>Disclosure</h4>
                    <p>Information and interactive calculators are made available to you as self-help tools for your independent use and are not intended to provide investment advice. We cannot and do not guarantee their applicability or accuracy in regards to your individual circumstances. All examples are hypothetical and are for illustrative purposes. We encourage you to seek personalized advice from qualified professionals regarding all personal finance issues.</p>
                </div>
            </div>
        </div>
    </div>