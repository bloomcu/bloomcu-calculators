<div
    id="calculator-<?php echo $modal_location; ?>-modal-<?php echo $idRand__modal; ?>"
    class="c-off-canvas c-off-canvas--small"
    data-modal="calculator-<?php echo $modal_location; ?>-modal-<?php echo $idRand__modal; ?>"
    aria-hidden="true"
>
    <div class="c-off-canvas__inner">
        <button class="c-off-canvas__close" data-close-modal aria-label="Close this information">
            <span class="c-off-canvas__close-icon">
                <span class="hide h-visual-hide">Close this information popup.</span>
                <i class="fal fa-times"></i>
            </span>
        </button>

        <div class="c-off-canvas__content">
            <h3 class="c-off-canvas__title" 
            id="calculator-<?php echo $modal_location; ?>-modal-header-<?php echo $idRand__modal; ?>">
                <?php echo $modal_header; ?>
            </h3>

            <div class="c-off-canvas__content-inner wysiwyg-content">
                <?php echo $modal_content; ?>
            </div>
        </div>
    </div>
</div>