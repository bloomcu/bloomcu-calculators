<div
    id="calculator_mortgage_payment_<?php echo $idRand; ?>_advanced"
    class="c-off-canvas c-off-canvas--small c-off-canvas--calculator"
    data-modal="calculator-mortgage-advanced-<?php echo $idRand; ?>"
    aria-hidden="true"
>
    <div class="c-off-canvas__inner">
        <button class="c-off-canvas__close" data-close-modal aria-label="Close this information">
            <span class="c-off-canvas__close-icon">
                <i class="fal fa-times"></i>
            </span>
        </button>

        <div class="c-off-canvas__content">
            <h3 class="c-off-canvas__title">
                <?php echo __('Advanced Options', 'bloomcu-calculators');?>
                
            </h3>

            <div class="c-off-canvas__content-inner wysiwyg-content">
                <h4><?php echo __('Property Tax Rate', 'bloomcu-calculators');?></h4>
                
                <p><?php echo __('The tax that you are required to pay as a property owner levied by the city or municipality. Lookup your county rate or state average rate for a more accurate calculation.', 'bloomcu-calculators');?></p>

                <div class="calculator-section">
                    <div class="input-group">
                        <label class="hide h-visual-hide" for="input-propery-tax-rate-<?php echo $idRand;?>"><?php echo __('Property Tax Rate', 'bloomcu-calculators');?></label>
                        <div class="pre">%</div>
                        <input type="text" class="calc-input input-property-tax-rate" value="1.00" id="input-propery-tax-rate-<?php echo $idRand;?>">
                    </div>
                </div>

                <h4><?php echo __('Homeowners Insurance (Monthly)', 'bloomcu-calculators');?></h4>
                <p><?php echo __('The standard insurance policy that covers damage to your property and the things you keep in it. The average annual homeowners insurance premium is around $1,200.', 'bloomcu-calculators');?></p>

                <div class="calculator-section">
                    <div class="input-group">
                        <label class="hide h-visual-hide" for="input-homeowners-insurance-<?php echo $idRand;?>"><?php echo __('Homeowners Insurance (Monthly)', 'bloomcu-calculators');?></label>
                        <div class="pre">$</div>
                        <input type="text" class="calc-input input-homeowners-insurance" value="100" id="input-homeowners-insurance-<?php echo $idRand;?>">
                    </div>
                </div>

                <h4><?php echo __('HOA Fees (Monthly)', 'bloomcu-calculators');?></h4>
                <p><?php echo __('These are dues that are used by a homeowners association toward maintenance of common areas used by all homeowners in a housing development or complex.', 'bloomcu-calculators');?></p>

                <div class="calculator-section">
                    <div class="input-group">
                        <label class="hide h-visual-hide" for="input-hoa-fee-<?php echo $idRand;?>"><?php echo __('HOA Fees (Monthly)', 'bloomcu-calculators');?></label>
                        <div class="pre">$</div>
                        <input type="text" class="calc-input input-hoa-fee" value="0" id="input-hoa-fee-<?php echo $idRand;?>">
                    </div>
                </div>

                <h4><?php echo __('Private Mortgage Insurance Rate', 'bloomcu-calculators');?></h4>
                <p><?php echo __('The premium for the insurance policy for FHA loans and conventional mortgages if your down payment is less than 20%. PMI typically costs between 0.5% to 1% of the entire loan amount on an annual basis. Only calculated if down payment is less than 20% of mortgage amount.', 'bloomcu-calculators');?></p>

                <div class="calculator-section">
                    <div class="input-group">
                        <label class="hide h-visual-hide" for="input-mortgage-insurance-<?php echo $idRand;?>"><?php echo __('Private Mortgage Insurance Rate', 'bloomcu-calculators');?></label>
                        <div class="pre">%</div>
                        <input type="text" class="calc-input input-mortgage-insurance" value="1.00" id="input-mortgage-insurance-<?php echo $idRand;?>">
                    </div>
                </div>

                <h4><?php echo __('Disclosure', 'bloomcu-calculators');?></h4>
                <p><?php echo __('This calculation represents an estimate. Calculation does not factor credit score. Default input values are estimates.', 'bloomcu-calculators');?></p>
            </div>
        </div>
    </div>
</div>