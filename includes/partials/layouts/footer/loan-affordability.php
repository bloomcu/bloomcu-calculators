<?php function get_loan_affordability_footer_layout( $id ) {
    /**
    * Footer
    * ----------
    */
    if (get_post_meta($id, 'footer_customize', true)) {
        render_footer(
            $id,
            $title = get_post_meta($id, 'footer_title', true),
            $btn_label = get_post_meta($id, 'footer_btn_label', true),
            $btn_url = get_post_meta($id, 'footer_btn_url', true),
            $disclosure = get_post_meta($id, 'footer_disclosure', true)
        );
    } else {
        render_footer(
            $id,
            $title = '',
            $btn_label = 'Apply Now',
            $btn_url = get_site_url() . '#apply-now',
            $disclosure = __('This calculation represents an estimate','bloomcu-calculators')
        );
    }
}