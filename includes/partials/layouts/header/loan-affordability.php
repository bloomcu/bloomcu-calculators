<?php function get_loan_affordability_header_layout($id) {
	if ( get_post_meta( $id, 'header_customize', true ) ) {

        render_header(
            $title = get_post_meta( $id, 'header_title', true )
        );

    } else {

        render_header(
            $title = 'Affordability',
            $subtitle = ''
        );

    }
}