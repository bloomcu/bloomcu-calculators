<?php function get_savings_compounding_header_layout( $id, $idRand ) { 
    
    $title = 'Final Balance';
    // Disable header acf fields
    // if( get_post_meta( $id, 'header_customize', true ) ){
    // 	$title = get_post_meta( $id, 'header_title', true );
    // }
?>
    <div class="calculator-header">
        <h3><?php echo __($title, 'bloomcu-calculators');?></h3>
        <div class="output-result"></div>
        <span><?php echo __('Contributions:', 'bloomcu-calculators');?> <span class="output-contribution"></span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>
        <span><?php echo __('Earnings:', 'bloomcu-calculators');?> <span class="output-earnings"></span></span>
    </div>
<?php }