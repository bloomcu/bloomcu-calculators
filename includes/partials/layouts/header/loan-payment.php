<?php function get_loan_payment_header_layout($id) {
	if ( get_post_meta( $id, 'header_customize', true ) ) {

		render_header(
			$title = get_post_meta( $id, 'header_title', true ),
			$subtitle = get_post_meta( $id, 'result_subtitle', true )
		);

	} else {

		render_header(
			$title = 'Monthly Payment',
			$subtitle = ''
		);

	}
}