<?php function get_mortgage_payment_header_layout( $id ){
/**
		* Header
		* ----------
		*/
		$title = 'Monthly Payment';
		if( get_post_meta( $id, 'header_customize', true ) ){
			$title = get_post_meta( $id, 'header_title', true );
		}
		// if ( get_post_meta( $id, 'header_customize', true ) ) {

		// 	render_header(
		// 		$title = get_post_meta( $id, 'header_title', true ),
		// 		$subtitle = get_post_meta( $id, 'result_subtitle', true )
		// 	);

		// } else {

		// 	render_header(
		// 		$title = 'Monthly Payment',
		// 		$subtitle = ''
		// 	);

		// }
		?>

		<div class="calculator-header">
			<span class='caculator-header__title h3'><?php echo $title;?></span>
			<div class="output-result"></div>
			<div class="calculator-header-left">
				<span><?php echo __('Property Tax:', 'bloomcu-calculators');?> <span class="output-property-tax">167</span></span>
				<br>
				<span><?php echo __('Homeowners Insurance:', 'bloomcu-calculators');?> <span class="output-homeowners-insurance">100</span></span>
			</div>
			<div class="calculator-header-right">
				<span><?php echo __('HOA Fee:', 'bloomcu-calculators');?> <span class="output-hoa-fee">0</span></span>
				<br>
				<span><?php echo __('Mortgage Insurance:', 'bloomcu-calculators');?> <span class="output-mortgage-insurance">167</span></span>
			</div>
		</div>

<?php }