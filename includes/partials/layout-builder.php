<?php
/**
 * Build Default layout
 *  
 * @param int $id Calculator post id
 * @param string $slug String representing the calculator type (e.g: loan_affordability)
 * @param array $idRand array of of component names 
 * @param int $idRand random int used for assigning a unique to calculators with modal options
 * 
 * @return null
 */

function get_calc_layout_builder($id, $slug, $layout_rows, $idRand = NULL) {
	$component_names = [
		'header',
		'footer',
		'body'
	];

	if( ! is_array( $layout_rows ) ) {
		return false;
	}
	
	foreach( $layout_rows as $row ) {
		$component = strtolower( $row['component'] );
		if( ! in_array( $component, $component_names)){
			return false;
		}
		if( function_exists( 'get_' . $slug . '_' . $component . '_layout' ) ) {
			call_user_func( 'get_' . $slug . '_' . $component . '_layout', $id, $idRand );
		}
	}
}


/**
 * Find and Assemble correct layout
 *  
 * @param int $id Calculator post id
 * @param string $slug String representing the calculator type (e.g: loan_affordability)
 * @param int $idRand random int used for assigning a unique to calculators with modal options
 * 
 * @return null
 */

function build_layout($id, $slug, $idRand = NULL){
	$customize_layout = get_field( 'customize_layout', $id );
	$layout_rows = get_field( 'layout_builder', $id);
    
	if( $customize_layout === true && count( $layout_rows ) ){
        
        get_calc_layout_builder( $id, $slug, $layout_rows, $idRand );
        
	} else {

        if( function_exists( 'get_' . $slug . '_header_layout' ) ){
            call_user_func( 'get_' . $slug . '_header_layout', $id, $idRand );
        }
        if( function_exists( 'get_' . $slug . '_body_layout' ) ){
            call_user_func('get_' . $slug . '_body_layout', $id, $idRand );
        }
        if( function_exists( 'get_' . $slug . '_footer_layout' ) ){
            call_user_func( 'get_' . $slug . '_footer_layout', $id, $idRand );
        }
	}
}