<?php
namespace Calculator;

/**
 * REST_API Handler
 */
class REST_API {

	public function __construct() {
		add_action( 'rest_api_init', function() {
			register_rest_route( 'calculators/v1', '/calculator/(?P<id>\d+)', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_calculator' ],
			] );
		} );
	}

	public function get_calculator( $data ) {
		$calculator = get_post( $data['id'] );

		// Bail if empty or not a calculator post
		if ( empty( $calculator ) || $calculator->post_type !== 'calculator' ) {
			return 0;
		}

		// attach calculator schema meta
		$schema = get_post_meta( $data['id'], '_bloom_calculator_schema', true );
		$calculator->calculator_structure = $schema;

		// attach calculator type
		$type = get_post_meta( $data['id'], '_bloom_calculator_type', true );
		$calculator->calculator_type = $type;

		return $calculator;
	}

}
