<?php
/**
 * Calculator: Saving Compounding Interest
 *
 */

function render_saving_compounding( $id ) {

	$layout = get_post_meta( $id, 'calculator_layout', true );
	$slug = get_post_meta( $id, 'calculator_type', true );
	$idRand = mt_rand(10000,99999);

	?>

	<div id="calculator_saving_compounding_<?php echo $idRand; ?>" class="calculator calculator-saving-compounding calculator-layout-<?php echo $layout; ?>">
		<?php build_layout( $id, $slug, $idRand ) ;?>
    </div>

	<?php
} ?>
