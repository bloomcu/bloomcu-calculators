<?php
/**
 * Calculator: Retirement Savings
 *
 */

function render_retirement_savings( $id ) {

	$layout = get_post_meta( $id, 'calculator_layout', true );
	$idRand = mt_rand(10000,99999);


	?>

	<div id="calculator_retirement_savings_<?php echo $idRand; ?>" class="calculator calculator-retirement-savings calculator-layout-<?php echo $layout; ?>">

        <div class="calculator-body">

			<div class="calculator-section">

			<?php $input_cur_age_ID = 'input-current-age-'.$idRand;?>

				<label for="<?php echo $input_cur_age_ID;?>" class="calc_main_input_desc">Current Age</label>
				<div class="input-group">
					<input type="text" class="calc-input input-current-age" value="30"
					id="<?php echo $input_cur_age_ID;?>">
				</div>
			</div>

			<div class="calculator-section">
			<?php $input_retirement_age = 'input-retirement-age-'.$idRand;?>

				<label for="<?php echo $input_retirement_age;?>">Retirement Age</label>
				<div class="input-group">
					<input type="text" class="calc-input input-retirement-age" value="67"
					id="<?php echo $input_retirement_age;?>">
				</div>
			</div>

            <?php

            /**
            * Amount
            * ----------
            */

        	if ( get_post_meta( $id, 'amount_customize', true ) ) {

				render_input(
					$id,
					$part    = 'amount',
					$label   = get_post_meta( $id, 'amount_label', true ),
					$type    = get_post_meta( $id, 'amount_type', true ),
					$value   = get_post_meta( $id, 'amount_placeholder', true ),
					$options = bcu_calc_get_options( $id, $part, $type, true )
				);

        	} else {

				render_input(
					$id,
					$part    = 'amount',
					$label   = 'Current Savings',
					$type    = 'Text',
					$value   = '100000',
					$options = ''
				);

            }

			?>

			<div class="calculator-section">

			<?php $input_annual_deposit = 'input-annual-deposit-'.$idRand;?>

				<label for="<?php echo $input_annual_deposit;?>" class="calc_main_input_desc">Annual Deposit</label>
				<div class="input-group">
					<div class="pre">$</div>
					<input type="text" class="calc-input input-annual-deposit" value="10000"
					id="<?php echo $input_annual_deposit;?>">
				</div>
			</div>

			<?php

			/**
            * Rate
            * ----------
            */

        	if ( get_post_meta( $id, 'rate_customize', true ) ) {

				render_input(
					$id,
					$part    = 'rate',
					$label   = get_post_meta( $id, 'rate_label', true ),
					$type    = get_post_meta( $id, 'rate_type', true ),
					$value   = get_post_meta( $id, 'rate_placeholder', true ),
					$options = bcu_calc_get_options( $id, $part, $type, true )
				);

        	} else {

				render_input(
					$id,
					$part    = 'rate',
					$label   = 'Interest Rate',
					$type    = 'Text',
					$value   = '8.00',
					$options = ''
				);

            }
			if( get_post_meta( $id, 'term_customize', true)) :
				render_input(
					$id,
					$part    = 'years-of-retirement',
					$label   = get_post_meta( $id, 'term_label', true ),
					$type    = get_post_meta( $id, 'term_type', true ),
					$value   = get_post_meta( $id, 'term_placeholder', true ),
					
					// TODO: $part is set to "years of retirement" to work with the calculate function as is
					// term is overriddn in the options var below

					// $options = bcu_calc_get_options( $id, $part, $type, true )
					$options = bcu_calc_get_options( $id, 'term', $type, true )
				);
			else : ?>

				<div class="calculator-section calculator-section--has-slider">

				<?php 
					$slider_input_retirement = 'slider_input_retirement-'.$idRand;
					$slider_output_retirement = 'slider_output_retirement-'.$idRand;
					?>

						<label for="<?php echo $slider_input_retirement; ?>" class="calc_main_input_desc">Years of Retirement Income</label>

						<span id="<?php echo $slider_output_retirement; ?>" class="slider-output slider-output-years"></span>

						<div class="slider-group">
							<input type="range"
									id="<?php echo $slider_input_retirement; ?>"
									class="calc-input input-years-of-retirement slider_input"
									name="slider"
									min="1"
									max="100"
									step="1"
									value="34">
								<div class="slider-group-min slider-group-min-years">
									1
								</div>
								<div class="slider-group-max slider-group-min-years">
									100
								</div>
						</div>

						<script type="text/javascript">
							var slider = document.getElementById("<?php echo $slider_input_retirement; ?>");
							var output = document.getElementById("<?php echo $slider_output_retirement; ?>");
							output.innerHTML = slider.value; // Display the default slider value

							// Update the current slider value (each time you drag the slider handle)
							slider.oninput = function() {
								output.innerHTML = this.value;
							}
							slider.onchange = function() {
								output.innerHTML = this.value;
							}
						</script>
					</div>
				<?php 
			endif;
            ?>

			

			<?php $input_inflation_label = 'input-inflation-'.$idRand;?>
			<?php $input_inflation_option_1 = 'input-inflation-1-'.$idRand;?>
			<?php $input_inflation_option_2 = 'input-inflation-2-'.$idRand;?>
			<fieldset class="calculator-section">

				<legend class="calc_main_input_desc">Adjust for inflation</legend>

				<div class="button-group">
						<span class="button-group__button">
							<input
								value="0"
								type="radio"
								name="inflation-options"
								class="calc-input input-inflation"
								id="<?php echo $input_inflation_option_1; ?>"
								checked
							>
							<label for="<?php echo $input_inflation_option_1; ?>">No</label>
						</span>

						<span class="button-group__button">
							<input
								value="1"
								type="radio"
								name="inflation-options"
								id="<?php echo $input_inflation_option_2; ?>"
								class="calc-input input-inflation"
							>
							<label for="<?php echo $input_inflation_option_2; ?>">Yes</label>
						</span>
					</fieldset>
			</fieldset>

			<span class="calculator-disclosure">
				Interest compounds yearly.
				<a class="calculator-modal-trigger" href="#calculator-retirement-modal-<?php echo $idRand; ?>">
					Read assumptions
				</a>
			</span>

        </div>

		<div class="calculator-header">
			<h3>Total Retirement Savings</h3>
			<div class="output-result output-result-savings"></div>
			<h3>Annual Retirement Income</h3>
			<div class="output-result output-result-income"></div>
		</div>

		<?php

		/**
		* Footer
		* ----------
		*/

		if ( get_post_meta( $id, 'footer_customize', true ) ) {

			render_footer(
				$id,
				$title = get_post_meta( $id, 'footer_title', true ),
			    $btn_label = get_post_meta( $id, 'footer_btn_label', true ),
			    $btn_url = get_post_meta( $id, 'footer_btn_url', true ),
			    $disclosure = get_post_meta( $id, 'footer_disclosure', true )
			);

		} else {

			render_footer(
				$id,
				$title = '',
			    $btn_label = 'Open Account',
			    $btn_url = get_site_url() . '#apply-now',
			    $disclosure = ''
			);

		}
		add_action('wp_footer', function() use ($idRand) {
			ob_start();
			include( CALCULATORPLUGIN_INCLUDES . '/partials/layouts/modals/retirement-savings.php' );
			echo ob_get_clean();
 
		})
		?>

		

	</div>


	<?php
} ?>
