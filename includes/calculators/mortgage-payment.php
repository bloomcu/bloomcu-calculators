<?php
/**
 * Calculator: Mortgage Payment
 *
 */

function render_mortgage_payment( $id ) {

	$layout = get_post_meta( $id, 'calculator_layout', true );
	$idRand = mt_rand(10000,99999);
	$slug = 'mortgage_payment';

?>

	<div id="calculator_mortgage_payment_<?php echo $idRand; ?>" class="calculator calculator-mortgage-payment calculator-layout-<?php echo $layout; ?>">
		<?php build_layout( $id, $slug, $idRand ) ;?>
    </div>

	<?php
} ?>
