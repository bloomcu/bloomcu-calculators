<?php
/**
 * Index
 * Entry point for all calculators.
 *
 */

$files = [
    'loan-payment.php',
    'mortgage-payment.php',
    'loan-affordability.php',
    'saving-compounding.php',
    'investment.php',
    'retirement-savings.php'
];

foreach ( $files as $file ) {
	require_once $file;
}
