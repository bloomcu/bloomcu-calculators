<?php
/**
 * Calculator: Investment
 *
 */

function render_investment( $id ) {

	$layout = get_post_meta( $id, 'calculator_layout', true );

	$idRand = mt_rand(10000,99999);

	?>

	<div id="calculator_investment_<?php echo $idRand; ?>" class="calculator calculator-investment calculator-layout-<?php echo $layout; ?>">

        <div class="calculator-body">

            <?php

			/**
            * Term
            * ----------
            */

        	if ( get_post_meta( $id, 'term_customize', true ) ) {

				render_input(
					$id,
					$part    = 'term',
					$label   = get_post_meta( $id, 'term_label', true ),
					$type    = get_post_meta( $id, 'term_type', true ),
					$value   = get_post_meta( $id, 'term_placeholder', true ),
					$options = bcu_calc_get_options( $id, $part, $type, true )
				);

        	} else {

				render_input(
					$id,
					$part    = 'term',
	                $label   = 'Years',
					$type    = 'Slider',
					$value   = '',
					$options = array(
						"min"=>"1",
						"max"=>"100",
						"step"=>"1",
						"val"=>"25",
					)
				);

            }

            /**
            * Amount
            * ----------
            */

        	if ( get_post_meta( $id, 'amount_customize', true ) ) {

				render_input(
					$id,
					$part    = 'amount',
					$label   = get_post_meta( $id, 'amount_label', true ),
					$type    = get_post_meta( $id, 'amount_type', true ),
					$value   = get_post_meta( $id, 'amount_placeholder', true ),
					$options = bcu_calc_get_options( $id, $part, $type, true )
				);

        	} else {

				render_input(
					$id,
					$part    = 'amount',
					$label   = 'Initial Investment',
					$type    = 'Text',
					$value   = '100000',
					$options = ''
				);

            }

			?>

			<div class="calculator-section">
			<?php $input_annual_contribution = 'input-annual-contribution-'.$idRand;?>
				<label 
				class="calc_main_input_desc"
				for="<?php echo $input_annual_contribution;?>">Annual Investment</label>
				<div class="input-group">
					<div class="pre">$</div>
					<input type="text" class="calc-input input-annual-contribution" value="2400"
					id="<?php echo $input_annual_contribution;?>">
				</div>
			</div>

			<?php

			/**
            * Rate
            * ----------
            */

        	if ( get_post_meta( $id, 'rate_customize', true ) ) {

				render_input(
					$id,
					$part    = 'rate',
					$label   = get_post_meta( $id, 'rate_label', true ),
					$type    = get_post_meta( $id, 'rate_type', true ),
					$value   = get_post_meta( $id, 'rate_placeholder', true ),
					$options = bcu_calc_get_options( $id, $part, $type, true )
				);

        	} else {

				render_input(
					$id,
					$part    = 'rate',
					$label   = 'Rate of Return',
					$type    = 'Text',
					$value   = '7.00',
					$options = ''
				);

            }

			?>

			<span class="calculator-disclosure">
				Interest compounds monthly.
				<a class="calculator-modal-trigger" href="#calculator-retirement-modal-<?php echo $idRand; ?>">
					Read Assumptions
				</a>
			</span>

        </div>

		<div class="calculator-header">
			<h3>Investment Total</h3>
			<div class="output-result"></div>
			<span>Contributions: <span class="output-contribution"></span></span>
			<span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Earnings: <span class="output-earnings"></span></span>
		</div>

		<?php

		/**
		* Footer
		* ----------
		*/

		if ( get_post_meta( $id, 'footer_customize', true ) ) {

			render_footer(
				$id,
				$title = get_post_meta( $id, 'footer_title', true ),
			    $btn_label = get_post_meta( $id, 'footer_btn_label', true ),
			    $btn_url = get_post_meta( $id, 'footer_btn_url', true ),
			    $disclosure = get_post_meta( $id, 'footer_disclosure', true )
			);

		} else {

			render_footer(
				$id,
				$title = '',
			    $btn_label = 'Apply Now',
			    $btn_url = get_site_url() . '#apply-now',
			    $disclosure = ''
			);

		}

		add_action('wp_footer', function() use ($idRand) {
			ob_start();
			include( CALCULATORPLUGIN_INCLUDES . '/partials/layouts/modals/investment.php' );
			echo ob_get_clean();
 
		})

		?>

		

    </div>

	<?php
} ?>
