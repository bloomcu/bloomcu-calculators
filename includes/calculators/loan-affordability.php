<?php
/**
 * Calculator: Loan Affordability
 *
 */

function render_loan_affordability( $id ) {

	$layout = get_post_meta( $id, 'calculator_layout', true );
	$slug = 'loan_affordability';
	?>

	<div id="calculator_afford_<?php echo mt_rand(); ?>" class="calculator calculator-afford calculator-layout-<?php echo $layout; ?>">

		<?php build_layout( $id, $slug ) ;?>

    </div>

	<?php
} 

