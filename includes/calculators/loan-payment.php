<?php
/**
 * Calculator: Loan Payment
 *
 */

function render_loan_payment( $id ) {
	$layout = get_post_meta( $id, 'calculator_layout', true ); //TODO: find out where this is used
	$slug = 'loan_payment';
	?>

	<div id="calculator_payment_<?php echo mt_rand(); ?>" class="calculator calculator-payment calculator-layout-<?php echo $layout; ?>"> 
		<?php build_layout( $id, $slug ) ;?>
    </div>

	<?php
}