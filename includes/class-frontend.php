<?php
namespace Calculator;
require 'utilities/index.php';
require 'partials/index.php';
require 'calculators/index.php';

/**
 * Frontend Pages Handler
 */
class Frontend {

	public function __construct() {
		add_shortcode( 'calculator', [ $this, 'render_frontend' ] );
	}

	/**
	 * Render frontend
	 *
	 * @param  array $atts
	 *
	 * @return string
	 */
	public function render_frontend( $atts ) {
		wp_enqueue_style( 'calculatorplugin-frontend' );
		wp_enqueue_script( 'calculatorplugin-frontend' );

		$calculator_atts = shortcode_atts( [
			'id'   => null,
			'type' => null,
		], $atts );

		// Grab shortcode attributes
		$id = $calculator_atts['id'];
		$type = $calculator_atts['type'];

		// Set selected type
		$selected_type = '';
		if ( isset( $type ) && !isset( $id ) ) {
			// Get the calculator type using the arg passed in available
			$selected_type = $type;
		} else {
			// Otherwise use what has been saved to the post
			$selected_type = get_post_meta( $id, 'calculator_type', true );
		}

		/**
		* Render calculator
		* -----------------
		*/

		// Start buffering calculator
		ob_start();

		/**
		* Loan Payment
		*/
		if ( $selected_type == 'loan_payment' ) {
			render_loan_payment( $id );
		}

		/**
		* Mortgage Payment
		*/
		if ( $selected_type == 'mortgage_payment' ) {
			render_mortgage_payment( $id );
		}

		/**
		* Loan Affordability
		*/
		if ( $selected_type == 'loan_affordability' ) {
			render_loan_affordability( $id );
		}

		/**
		* Savings Compounding
		*/
		if ( $selected_type == 'savings_compounding' ) {
			render_saving_compounding( $id );
		}

		/**
		* Investment
		*/
		if ( $selected_type == 'investment' ) {
			render_investment( $id );
		}

		/**
		* Retirement Savings
		*/
		if ( $selected_type == 'retirement_savings' ) {
			render_retirement_savings( $id );
		}
		render_slider_scripts();
		// Return contents of buffer
		$language_class = '';
		if( defined( 'ICL_LANGUAGE_CODE' ) ){
			$language_class = 'bloomcu_calculator--lang-' . ICL_LANGUAGE_CODE;
		}
		return '<div class="bloomcu-calculator ' . $language_class . '">' . ob_get_clean() . '</div>'; // Return contents of buffer
		// return ob_get_clean();
	}

}
