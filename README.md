# BloomCU Calculators

Customizable financial calculators. A WordPress Plugin by BloomCU.

## Changelog

**1.8.9**
*Release Date 7 Nov 2023*
* Fix function call error with WP 6.4.x

**1.8.7**
*Release Date 14 Sep 2021*
* Update footer partial to conditionally render button when button label is provided

**1.8.6**
*Release Date 29 April 2021*
* Update to add labels to advance options fields on home mortgage calculator

**1.8.5**
*Release Date 16 April 2021*
* update to fix arial labeledby id on radio inputs

**1.8.3/1.8.4**
*Release Date 15 September 2020*
* update to fix arial labeledby id on radio inputs

**1.8.2**
*Release Date 15 September 2020*
* update asset class to check get_current_screen exists

**1.8.1**
*Release Date 15 September 2020*
* ADA compliance - Mortgage calc - remove h3

**1.8.0**
*Release Date 3 September 2020*
* Move rate modal to footer to fix issues with overlay sitting on top of modal content

**1.7.6**
*Release Date 22 July 2020*
* add lang class to calculator body if icl_language_code is set


**1.7.5**
*Release Date 22 July 2020*
* Retirement savings term - allow template override


**1.7.4**
*Release Date 20 July 2020*
* inject calculator modals into page footer

**1.7.3**
*Release Date 5 May 2020*
* ADA fix for calculator headers

**1.7.2**
*Release Date 16 April 2020*
* ADA fix for calculator headers

**1.7.1**
*Release Date 12 March 2020*
* Remove extra character from investment

**1.7.0**
*Release Date = 25 February 2020*
* Allow markup sections to be reordered
* Enable string translations on hardcoded template strings

**1.6.3**
*Release Date = 29 November 2019*
* Restructure unique IDs for all other calculator types.

**1.6.2**
*Release Date = 29 November 2019*
* Fixed duplicate IDs with some form fields, allowing multiple of the same calculator type to be shown on same page.

**1.6.1**
*Release Date = 27 November 2019*
* Fixed issue with retirement income years slider not updating output value if multiple sliders on page
* Set maxlength on rates field to 5

**1.6.0**
*Release Date = 25 November 2019*
* Separate calculators JS scopes
* Cleanup JS

**1.5.1**
*Release Date = 27 October 2019*
* Minor updated to Compounding Savings calculator

**1.5.0**
*Release Date = 17 October 2019*
* Added mortgage calculator
* Made improvements to all calculator disclosures

**1.4.2**
*Release Date = 17 October 2019*
* Added result subheader to payment calculator

**1.4.1**
*Release Date – 16 October 2019*
* Updated default cta url
* Moved and update disclosures
* Made minor updates to default styles

**1.4.0**
*Release Date – 14 October 2019*
* Added retirement calculator
* Added investment calculator

**1.3.3**
*Release Date – 8 October 2019*
* Scoped webpack to a library

**1.3.2**
*Release Date – 1 October 2019*
* Formatted all output results with commas
* Cleaned up calculator script

**1.3.0**
*Release Date – 26 September 2019*
* Added compounding interest saving calculator
* Loosened up styles on calculator heading section
* Organized stylesheets by components and calculators

**1.2.1**
*Release Date – 9 April 2019*
* Added optional model option to footer and rate input
* Moved input symbol before input
* Set first option in button group to selected

**1.2.0**
*Release Date – 9 March 2019*
* Added calculator layout option

**1.1.0**
*Release Date – 24 February 2019*
* Added Affordability Calculator

**1.0.0**
*Release Date – 16 January 2019*
* Initial release of plugin
* Included Payment Calculator
