/*
 * Format Number
 * ----
 * This function is used to add thousand seperators to numerical ouput
 * as a means of properly formatting money
 */
function formatNumber(num) {
    num = num.toFixed()
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

/*
 * Get Input Value
 * ----
 * This function checks the type of input being used and gets its value
 */
function getInputValue(inputClass) {

    // Slider
    if ( $(inputClass).is('input[type=range]') ) {
        return Number($(inputClass).val());

    // Buttons
    } else if ( $(inputClass).is('input:radio') ) {
        return Number($(inputClass + ':checked' ).val());

    // Input or Select
    } else if ( $(inputClass).is('input:text') || $(inputClass).is('select') ) {
        return Number($(inputClass).val().replace(/\,/g,''));
    }
}

/*
 * Loan Payment
 * ----
 * This function determines your monthly payment
 * using as ammount, rate and term.
 */
function calculate_payment(calc_id) {

    // Get inputs
    var amount = getInputValue(calc_id + ' .input-amount'),
        rate = getInputValue(calc_id + ' .input-rate'),
        term = getInputValue(calc_id + ' .input-term');
    
    // Calculate interest
    var monthly_rate = rate / 100 / 12;

    // Calculate discount factor
    // https://en.wikipedia.org/wiki/Discounting#Discount_factor
    var discount_factor = Math.pow( 1 + monthly_rate, term );

    // Calculate monthly payment
    var monthly_payment = ( amount * discount_factor * monthly_rate )/( discount_factor - 1 );
   
    // Format results with comma
    
 
    //catch Nan error
    if( isNaN( monthly_payment) ) {
        monthly_payment = 0;
    }
    
    monthly_payment = formatNumber(monthly_payment);
    // Output result
    $(calc_id + ' .output-result').html( '$' + monthly_payment + '<span class="results_ticker">/mo</span>' );
}

/*
 * Mortgage Payment
 * ----
 * This function determines your monthly mortgage payment
 * using an amount, down payment, rate, term, property taxes,
 * homeowners insurance and private mortgage insurance.
 */
function calculate_mortgage_payment(calc_id, modal_id) {
    // Get inputs
    var amount                  = getInputValue(calc_id + ' .input-amount'),
        downpayment             = getInputValue(calc_id + ' .input-down-payment'),
        principle               = amount - downpayment,
        rate                    = getInputValue(calc_id + ' .input-rate'),
        years                   = getInputValue(calc_id + ' .input-term'),
        months                  = years * 12,

        // advanced options
        property_tax_rate       = getInputValue(modal_id + ' .input-property-tax-rate'),
        homeowners_insurance    = getInputValue(modal_id + ' .input-homeowners-insurance'),
        hoa_fee                 = getInputValue(modal_id + ' .input-hoa-fee'),
        mortgage_insurance_rate = getInputValue(modal_id + ' .input-mortgage-insurance');


    // Calculate monthly interest rate
    var monthly_rate = rate / 100 / 12;

    // Calculate discount factor
    // https://en.wikipedia.org/wiki/Discounting#Discount_factor
    var discount_factor = Math.pow( 1 + monthly_rate, months );

    // Calculate monthly payment
    var monthly_payment = ( principle * discount_factor * monthly_rate )/( discount_factor - 1 );

    // Calculate monthly cost of property tax rate
    var monthly_property_tax = (property_tax_rate / 100) * amount / 12;

    // Calculate monthly cost of private mortgage insurance rate
    // If down payment is less than 20%
    var downpayment_percentage = (downpayment * 100) / amount;
    if (downpayment_percentage < 20) {
        var monthly_mortgage_insurance = (mortgage_insurance_rate / 100) * amount / 12;
    } else {
        var monthly_mortgage_insurance = 0;
    }

    // Calculate final monthly payment
    monthly_payment = monthly_payment + monthly_property_tax + homeowners_insurance + hoa_fee + monthly_mortgage_insurance;

    // Format results with comma
    monthly_payment = formatNumber(monthly_payment);
    monthly_property_tax = formatNumber(monthly_property_tax);
    monthly_mortgage_insurance = formatNumber(monthly_mortgage_insurance);

    // Output results
    $(calc_id + ' .output-result').html( '$' + monthly_payment + '/mo' );
    $(calc_id + ' .output-property-tax').html( '$' + monthly_property_tax );
    $(calc_id + ' .output-homeowners-insurance').html( '$' + homeowners_insurance );
    $(calc_id + ' .output-hoa-fee').html( '$' + hoa_fee );
    $(calc_id + ' .output-mortgage-insurance').html( '$' + monthly_mortgage_insurance );

}

/*
 * Loan Affordability
 * ----
 * This function determines how much money you can afford to borrow
 * using a payment ammount, rate and term.
 */
function calculate_afford(calc_id) {

    // Get inputs
    var payment = getInputValue(calc_id + ' .input-amount'),
        rate    = getInputValue(calc_id + ' .input-rate'),
        term    = getInputValue(calc_id + ' .input-term');

    // Calculate interest
    var monthly_rate = rate / 100 / 12,
        annual_rate = rate / 100;

    // Calculate amount result
    var amount = payment * (1 - Math.pow(1 + monthly_rate, -1 * term)) * (12/(annual_rate));

    // Format results with comma
    amount = formatNumber(amount);

    // Output result
    $(calc_id + ' .output-result').html( '$' + amount );
}

/*
 * Saving Compounding
 * ----
 * This function determines your savings
 * with compounding interest
 */

function calculate_saving_compounding(calc_id) {

    // Get inputs
    var amount               = getInputValue(calc_id + ' .input-amount'),
        annual_rate          = getInputValue(calc_id + ' .input-rate'),
        monthly_rate         = annual_rate / 12 / 100,
        months               = getInputValue(calc_id + ' .input-term'),
        monthly_contribution = getInputValue(calc_id + ' .input-monthly-contribution'),
        term                 = $(calc_id).find('.calculator-body').attr('data-term-as');

        if( term === 'years') {
            months *= 12;
        }

    // Calculate total contribution
    var total_contribution = amount + (months * monthly_contribution);

    // Set iterators
    var month_count = 0,
        year_count = 0;

    // Empty schedule
    // $('.calculator-saving-compounding .output-schedule').html('');

    // Iterate over each month of the year
    for (var i = 0; i <= months -1; i++) {

        // Calculate interest on amount and deposit
        amount = amount * (1 + monthly_rate) + ( monthly_contribution * (1 + monthly_rate));

        month_count++;

        // if (month_count == 12) {
        //     year_count++;
        //     month_count = 0;
        //
        //     // Schedule: Append ammount for this year to schedule
        //     // console.log( 'Year ' + year_count + ': ' + formatNumber(amount) );
        //
        // }
    }

    // Calculate earnings
    var earnings = amount - total_contribution;

    // Format results with comma
    amount = formatNumber(amount);
    total_contribution = formatNumber(total_contribution);
    earnings = formatNumber(earnings);

    // Output results
    $(calc_id + ' .output-result').html( '$' + amount );
    $(calc_id + ' .output-contribution').html( '$' + total_contribution );
    $(calc_id + ' .output-earnings').html ( '$' + earnings );

}

/*
 * Investment
 * ----
 * This function determines your investment earnings
 * with yearly compounding interest
 */
function calculate_investment(calc_id) {

    // Get inputs
    var amount               = getInputValue(calc_id + ' .input-amount'),
        annual_rate          = getInputValue(calc_id + ' .input-rate'),
        annual_rate          = annual_rate / 100,
        years                = getInputValue(calc_id + ' .input-term'),
        months               = years * 12,
        annual_contribution  = getInputValue(calc_id + ' .input-annual-contribution');

    // Calculate total contribution
    var total_contribution = amount + (years * annual_contribution);

    // Iterate over each year
    for (var i = 0; i < years; i++) {

        // Calculate interest on amount and deposit
        amount = amount * (1 + annual_rate) + ( annual_contribution * (1 + annual_rate));

    }

    // Calculate earnings
    var earnings = amount - total_contribution;

    // Format results with comma
    amount = formatNumber(amount);
    total_contribution = formatNumber(total_contribution);
    earnings = formatNumber(earnings);

    // Output results
    $(calc_id + ' .output-result').html( '$' + amount );
    $(calc_id + ' .output-contribution').html( '$' + total_contribution );
    $(calc_id + ' .output-earnings').html ( '$' + earnings );

}

/*
 * Retirement Saving
 * ----
 * This function determines your retirement income
 * with compounding interest and inflation
 */
function calculate_retirement_savings(calc_id) {

    // Get inputs
    var current_age       = getInputValue(calc_id + ' .input-current-age'),
        retirement_age    = getInputValue(calc_id + ' .input-retirement-age'),
        amount            = getInputValue(calc_id + ' .input-amount'),
        annual_deposit    = getInputValue(calc_id + ' .input-annual-deposit'),
        rate              = getInputValue(calc_id + ' .input-rate'),
        annual_rate       = rate / 100,
        years             = retirement_age - current_age,
        use_inflation     = getInputValue(calc_id + ' .input-inflation'),
        retirement_years  = getInputValue(calc_id + ' .input-years-of-retirement'),
        retirement_saving = amount;

        var count_years = 0;

    // Calculate total contribution
    var total_contribution = amount + (years * annual_deposit);

    // Calculate total retirement savings
    for (var i = 0; i < years; i++) {

        if (use_inflation) {

            // Reduce ammount based on 2.9% inflation rate
            // Years into the future is total years minus one until retirement age reached
            retirement_saving = retirement_saving / ( Math.pow( 1 + 0.029, years - (years - 1) ) );

            // Reduce deposit based on 2.9% inflation rate
            annual_deposit = annual_deposit / ( Math.pow( 1 + 0.029, years - (years - 1) ) );

        }

        // Calculate total savings
        retirement_saving = retirement_saving * (1 + annual_rate) + ( annual_deposit * (1 + annual_rate));

    }

    // Calculate annual retirement income
    for (var i = 0; i < years; i++) {

        count_years++;

        if (use_inflation) {

            // Reduce ammount based on 2.9% inflation rate
            // Years into the future is total years minus one until retirement age reached
            amount = amount / ( Math.pow( 1 + 0.029, years - (years - 1) ) );

            // Reduce deposit based on 2.9% inflation rate
            annual_deposit = annual_deposit / ( Math.pow( 1 + 0.029, years - (years - 1) ) );

        }

        // Calculate interest on amount and deposit
        amount = amount * (1 + annual_rate) + ( annual_deposit * (1 + annual_rate));

    }

    // Calculate annual retirement income
    var annual_retirement_income = amount / retirement_years;

    // Format results with comma
    retirement_saving = formatNumber(retirement_saving);
    annual_retirement_income = formatNumber(annual_retirement_income);

    // Output results
    $(calc_id + ' .output-result-savings').html( '$' + retirement_saving );
    $(calc_id + ' .output-result-income').html( '$' + annual_retirement_income );

}

/*
 * Initialize All Calculators
 * ---
 *
 *
 */
$(document).ready(function() {

    /*
     * Initialize Payment Calculator
     * ---
     */
    (function() {
        if ( $( '.calculator-payment' ).length ) {

            var calculators = $( '.calculator-payment' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                // Init the calculator
                calculate_payment(id);

                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_payment(id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_payment(id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_payment(id);
                });

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Loan Payment Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Initialize Mortgage Payment Calculator
     * ---
     */
    (function() {
        if ( $( '.calculator-mortgage-payment' ).length ) {

            var calculators = $( '.calculator-mortgage-payment' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                var modal_id = id + '_advanced';

                // Init the calculator
                calculate_mortgage_payment(id, modal_id);

                var property_tax_rate       = modal_id + ' .input-property-tax-rate',
                homeowners_insurance    = modal_id + ' .input-homeowners-insurance',
                hoa_fee                 = modal_id + ' .input-hoa-fee',
                mortgage_insurance_rate = modal_id + ' .input-mortgage-insurance';
                
                // Add listeners to inputs that exist outside the body of the calculator
                $( property_tax_rate )
                    .add( homeowners_insurance )
                    .add( hoa_fee )
                    .add( mortgage_insurance_rate )
                    .on( 'keyup', function(){
                        calculate_mortgage_payment(id, modal_id);
                    });               
                
                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_mortgage_payment(id, modal_id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_mortgage_payment(id, modal_id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_mortgage_payment(id, modal_id);
                });

                 // advanced options
        

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Mortgage Payment Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Initialize Affordability Calculator
     * ---
     */
    (function() {
        if ( $( '.calculator-afford' ).length ) {

            var calculators = $( '.calculator-afford' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                // Init the calculator
                calculate_afford(id);

                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_afford(id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_afford(id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_afford(id);
                });

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Loan Affordability Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Initialize Saving Compounding
     * ---
     */
    (function() {
        if ( $( '.calculator-saving-compounding' ).length ) {

            var calculators = $( '.calculator-saving-compounding' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                // Init the calculator
                calculate_saving_compounding(id);

                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_saving_compounding(id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_saving_compounding(id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_saving_compounding(id);
                });

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Saving Compounding Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Initialize Investment
     * ---
     */
    (function() {
        if ( $( '.calculator-investment' ).length ) {

            var calculators = $( '.calculator-investment' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                // Init the calculator
                calculate_investment(id);

                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_investment(id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_investment(id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_investment(id);
                });

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Investment Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Initialize Retirement Savings
     * ---
     */
    (function() {
        if ( $( '.calculator-retirement-savings' ).length ) {

            var calculators = $( '.calculator-retirement-savings' );

            var bindEvents = function(calculator) {
                var id = '#' + calculator.id;

                // Init the calculator
                calculate_retirement_savings(id);

                // Listen for radio buttons selection
                $(".calc-input:radio", calculator).change(function() {
                    calculate_retirement_savings(id);
                });

                // Listen for all other inputs
                $(".calc-input", calculator).on('input', function() {
                    calculate_retirement_savings(id);
                });

                // Listen for all other changes
                $(".calc-input", calculator).on('change', function() {
                    calculate_retirement_savings(id);
                });

            };

            var init = function() {
                for (var i = 0 ; i < calculators.length; i++) {
                    bindEvents(calculators[i]);
                    console.info('Initialized Retirement Savings Calculator');
                }
            };

            init();
        }
    })();

    /*
     * Calculators Input Focus
     * ---
     * Select all content within input when you focus on input
     * Applied to all calculator text inputs
     */
    $('.calc-input:text').focus(function() {
        $(this).select();
    });

});
